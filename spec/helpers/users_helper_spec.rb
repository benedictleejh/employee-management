require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the UsersHelper. For example:
#
# describe UsersHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe UsersHelper, type: :helper do
  describe "make_sort_string" do
    it "should generate the correct symbol for a sort order" do
      expect(helper.make_sort_string("name", :asc)).to eq "+name"
      expect(helper.make_sort_string("name", :desc)).to eq "-name"
    end
  end

  describe "sorted_users_path" do
    it "should generate a descending sort for a current ascending sort" do
      expect(helper.sorted_users_path({}, "name", :asc, "name")).to eq users_path(sort: "-name")
    end

    it "should generate an ascending sort for a current descending sort" do
      expect(helper.sorted_users_path({}, "name", :desc, "name")).to eq users_path(sort: "+name")
    end

    it "should generate an ascending sort for a different column" do
      expect(helper.sorted_users_path({}, "name", :asc, "id")).to eq users_path(sort: "+id")
      expect(helper.sorted_users_path({}, "name", :desc, "id")).to eq users_path(sort: "+id")
    end

    it "should exclude the page parameter" do
      expect(helper.sorted_users_path({"page" => 1}, "name", :asc, "name")).to eq users_path(sort: "-name")
      expect(helper.sorted_users_path({"page" => 1}, "name", :asc, "id")).to eq users_path(sort: "+id")
      expect(helper.sorted_users_path({"page" => 1, "hello" => 1}, "name", :asc, "id")).to eq users_path(sort: "+id", hello: 1)
    end

    it "should include other query parameters" do
      expect(helper.sorted_users_path({"hello" => 1}, "name", :asc, "name")).to eq users_path(hello: 1, sort: "-name")
      expect(helper.sorted_users_path({"hello" => 1}, "name", :asc, "id")).to eq users_path(hello: 1, sort: "+id")
      expect(helper.sorted_users_path({"page" => 1, "hello" => 1}, "name", :asc, "id")).to eq users_path(sort: "+id", hello: 1)
    end
  end

  describe "form_heading" do
    it "should generate the right heading for the right symbol" do
      expect(helper.form_heading(:edit)).to eq "Edit employee"
      expect(helper.form_heading(:new)).to eq "Add a new employee"
    end

    it "should error when receiving an unknown symbol" do
      expect { helper.form_heading(:a) }.to raise_error(NoMatchingPatternError)
    end
  end
end
