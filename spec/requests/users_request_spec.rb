require 'rails_helper'

RSpec.describe "Users", type: :request do
  def response_body
    JSON.parse(response.body)
  end

  describe "POST /upload" do
    context "correctly formatted file" do
      it "should return success" do
        post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/correct.csv", 'text/csv') }

        expect(response).to have_http_status(200)
        expect(response).to be_successful
      end

      context "with comments" do
        it "should return success" do
          post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/correct-with-comments.csv", 'text/csv') }

          expect(response).to have_http_status(200)
          expect(response).to be_successful
        end
      end

      describe "should update an existing user" do
        let!(:user) { FactoryBot.create(:user, id: "e0001") }

        it "performs an update" do
          post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/correct.csv", 'text/csv') }
          get users_path, params: { minSalary: 0, maxSalary: 9999999, offset: 0, limit: 30, sort: "+id" }, as: :json

          expect(response_body['results'].first['login']).not_to eq user.login
          expect(response_body['results'].first['login']).to eq "jweissnat"
        end
      end
    end

    context "empty file" do
      it "should not return success" do
        post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/empty.csv", 'text/csv') }

        expect(response).not_to be_successful
      end
    end

    context "file with entries with incorrect column number" do
      context "too many columns" do
        it "should not return success" do
          post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/too-many-columns.csv", 'text/csv') }

          expect(response).not_to be_successful
        end
      end

      context "too few columns" do
        it "should not return success" do
          post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/too-few-columns.csv", 'text/csv') }

          expect(response).not_to be_successful
        end
      end
    end

    context "file with some incorrectly formatted salaries" do
      it "should not return success" do
        post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/wrong-salary-format.csv", 'text/csv') }

        expect(response).not_to be_successful
      end
    end

    context "file with negative salaries" do
      it "should not return success" do
        post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/negative-salary.csv", 'text/csv') }

        expect(response).not_to be_successful
      end
    end

    context "file with duplicates" do
      context "duplicated ids" do
        it "should not return success" do
          post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/duplicated-ids.csv", 'text/csv') }

          expect(response).not_to be_successful
        end
      end

      context "duplicated logins" do
        it "should not return success" do
          post upload_users_path, params: { file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/fixtures/duplicated-logins.csv", 'text/csv') }

          expect(response).not_to be_successful
        end
      end
    end
  end

  describe "GET /users" do
    let!(:user) { FactoryBot.create(:user) }

    context "with correctly formatted parameters" do
      it "should return the list of users" do
        @expected = {
          results: [
            {
              id: user.id,
              name: user.name,
              login: user.login,
              salary: user.salary
            }
          ]
        }.to_json
        get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, limit: 30, sort: "+name" }, as: :json

        expect(response).to be_successful
        expect(response.body).to eq(@expected)
      end

      context "string parameters" do
        context "minSalary" do
          it "should return the list of users" do
            @expected = {
              results: [
                {
                  id: user.id,
                  name: user.name,
                  login: user.login,
                  salary: user.salary
                }
              ]
            }.to_json
            get users_path, params: { minSalary: 0.to_s, maxSalary: 100000, offset: 0, limit: 30, sort: "+name" }, as: :json

            expect(response).to be_successful
            expect(response.body).to eq(@expected)
          end
        end

        context "maxSalary" do
          it "should return the list of users" do
            @expected = {
              results: [
                {
                  id: user.id,
                  name: user.name,
                  login: user.login,
                  salary: user.salary
                }
              ]
            }.to_json
            get users_path, params: { minSalary: 0, maxSalary: 100000.to_s, offset: 0, limit: 30, sort: "+name" }, as: :json

            expect(response).to be_successful
            expect(response.body).to eq(@expected)
          end
        end

        context "offset" do
          it "should return the list of users" do
            @expected = {
              results: [
                {
                  id: user.id,
                  name: user.name,
                  login: user.login,
                  salary: user.salary
                }
              ]
            }.to_json
            get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0.to_s, limit: 30, sort: "+name" }, as: :json

            expect(response).to be_successful
            expect(response.body).to eq(@expected)
          end
        end

        context "limit" do
          it "should return the list of users" do
            @expected = {
              results: [
                {
                  id: user.id,
                  name: user.name,
                  login: user.login,
                  salary: user.salary
                }
              ]
            }.to_json
            get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, limit: 30.to_s, sort: "+name" }, as: :json

            expect(response).to be_successful
            expect(response.body).to eq(@expected)
          end
        end
      end
    end

    context "with missing parameters" do
      context "with no minSalary" do
        it "should return HTTP status 400" do
          get users_path, params: { maxSalary: 100000, offset: 0, limit: 30, sort: "+name" }, as: :json

          expect(response).to have_http_status(400)
          expect(response).not_to be_successful
        end
      end

      context "with no maxSalary" do
        it "should return HTTP status 400" do
          get users_path, params: { minSalary: 0, offset: 0, limit: 30, sort: "+name" }, as: :json

          expect(response).to have_http_status(400)
          expect(response).not_to be_successful
        end
      end

      context "with no offset" do
        it "should return HTTP status 400" do
          get users_path, params: { minSalary: 0, maxSalary: 100000, limit: 30, sort: "+name" }, as: :json

          expect(response).to have_http_status(400)
          expect(response).not_to be_successful
        end
      end

      context "with no limit" do
        it "should return HTTP status 400" do
          get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, sort: "+name" }, as: :json

          expect(response).to have_http_status(400)
          expect(response).not_to be_successful
        end
      end

      context "with no sort" do
        it "should return HTTP status 400" do
          get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, limit: 30 }, as: :json

          expect(response).to have_http_status(400)
          expect(response).not_to be_successful
        end
      end
    end

    context "with incorrectly formatted parameters" do
      context "minSalary" do
        context "non-numeric" do
          it "should return HTTP status 400" do
            get users_path, params: { minSalary: 'a', maxSalary: 100000, offset: 0, limit: 30, sort: "+name" }, as: :json

            expect(response).to have_http_status(400)
            expect(response).not_to be_successful
          end
        end

        context "greater than maxSalary" do
          it "should return HTTP status 400" do
            get users_path, params: { minSalary: 100, maxSalary: 1, offset: 0, limit: 30, sort: "+name" }, as: :json

            expect(response).to have_http_status(400)
            expect(response).not_to be_successful
          end
        end
      end

      context "maxSalary" do
        context "non-numeric" do
          it "should return HTTP status 400" do
            get users_path, params: { minSalary: 0, maxSalary: 'a', offset: 0, limit: 30, sort: "+name" }, as: :json

            expect(response).to have_http_status(400)
            expect(response).not_to be_successful
          end
        end
      end

      context "offset" do
        context 'non-numeric' do
          it "should return HTTP status 400" do
            get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 'a', limit: 30, sort: "+name" }, as: :json

            expect(response).to have_http_status(400)
            expect(response).not_to be_successful
          end
        end

        context "negative" do
          it "should return HTTP status 400" do
            get users_path, params: { minSalary: 0, maxSalary: 100000, offset: -1, limit: 30, sort: "+name" }, as: :json

            expect(response).to have_http_status(400)
            expect(response).not_to be_successful
          end
        end
      end

      context "limit" do
        context "not 30" do
          it "should return HTTP status 400" do
            get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, limit: 1, sort: "+name" }, as: :json

            expect(response).to have_http_status(400)
            expect(response).not_to be_successful
          end
        end

        context "non-numeric" do
          it "should return HTTP status 400" do
            get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, limit: 'a', sort: "+name" }, as: :json

            expect(response).to have_http_status(400)
            expect(response).not_to be_successful
          end
        end
      end

      context "sort" do
        context "multiple keys" do
          it "should return HTTP status 400" do
            get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, limit: 30, sort: "+name+salary" }, as: :json

            expect(response).to have_http_status(400)
            expect(response).not_to be_successful
          end
        end

        context "no order" do
          context "incorrect initial symbol" do
            it "should return HTTP status 400" do
              get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, limit: 30, sort: "*name" }, as: :json

              expect(response).to have_http_status(400)
              expect(response).not_to be_successful
            end
          end

          context "no symbol" do
            it "should return HTTP status 400" do
              get users_path, params: { minSalary: 0, maxSalary: 100000, offset: 0, limit: 30, sort: "name" }, as: :json

              expect(response).to have_http_status(400)
              expect(response).not_to be_successful
            end
          end
        end
      end
    end
  end

  describe "POST /users" do
    let!(:user_attrs) { FactoryBot.attributes_for(:user) }

    context "with valid attributes" do
      it "has a successful response with HTTP status 200" do
        post users_path, params: { user: user_attrs }, as: :json

        expect(response).to have_http_status(200)
        expect(response).to be_successful
      end

      it "creates a new user" do
        expect {
          post users_path, params: { user: user_attrs }, as: :json
        }.to change { User.count }.by(1)
      end
    end

    context "with non-unique id" do
      before { FactoryBot.create(:user, id: user_attrs[:id]) }

      it "has an unsuccessful response with HTTP status 400" do
        post users_path, params: { user: user_attrs }, as: :json

        expect(response).to have_http_status(400)
        expect(response).not_to be_successful
      end

      it "does not create a new user" do
        expect {
          post users_path, params: { user: user_attrs }, as: :json
        }.not_to change { User.count }
      end
    end

    context "with non-unique login" do
      before { FactoryBot.create(:user, login: user_attrs[:login]) }

      it "has an unsuccessful response with HTTP status 400" do
        post users_path, params: { user: user_attrs }, as: :json

        expect(response).to have_http_status(400)
        expect(response).not_to be_successful
      end

      it "does not create a new user" do
        expect {
          post users_path, params: { user: user_attrs }, as: :json
        }.not_to change { User.count }
      end
    end

    context "with negative salary" do
      before { user_attrs[:salary] = -1 }

      it "has an unsuccessful response with HTTP status 400" do
        post users_path, params: { user: user_attrs }, as: :json

        expect(response).to have_http_status(400)
        expect(response).not_to be_successful
      end

      it "does not create a new user" do
        expect {
          post users_path, params: { user: user_attrs }, as: :json
        }.not_to change { User.count }
      end
    end
  end

  describe "GET /users/:id" do
    let!(:user) { FactoryBot.create(:user) }

    context "with valid id" do
      it "has a successful response with HTTP status 200" do
        get user_path(user.id), as: :json

        expect(response).to have_http_status(200)
        expect(response).to be_successful
      end

      it "returns the user as JSON" do
        expected = {
          "id" => user.id,
          "name" => user.name,
          "login" => user.login,
          "salary" => user.salary
        }.to_json
        get user_path(user.id), as: :json

        expect(response.body).to eq expected
      end
    end

    context "with invalid id", :realistic_error_responses do
      let!(:unknown_user) { FactoryBot.attributes_for(:user) }

      it "has an unsuccessful response with HTTP status 404" do
        get user_path(unknown_user[:id]), as: :json

        expect(response).to have_http_status(404)
        expect(response).not_to be_successful
      end
    end
  end

  describe "PATCH /users/:id" do
    let!(:user) { FactoryBot.create(:user) }
    let!(:user_attrs) { user.attributes }

    context "with valid attributes" do
      context "login" do
        before { user_attrs[:login] = "jdoe" }

        it "has a successful response with HTTP status 200" do
          patch user_path(user.id), params: { user: user_attrs }, as: :json

          expect(response).to have_http_status(200)
          expect(response).to be_successful
        end

        it "updates the user" do
          patch user_path(user.id), params: { user: user_attrs }, as: :json
          get user_path(user.id), as: :json

          expect(response_body["login"]).to eq user_attrs[:login]
        end
      end

      context "salary" do
        before { user_attrs[:salary] = 12345.12 }

        it "has a successful response with HTTP status 200" do
          patch user_path(user.id), params: { user: user_attrs }, as: :json

          expect(response).to have_http_status(200)
          expect(response).to be_successful
        end

        it "updates the user" do
          patch user_path(user.id), params: { user: user_attrs }, as: :json
          get user_path(user.id), as: :json

          expect(response_body["salary"]).to eq user_attrs[:salary].to_s
        end
      end
    end

    context "with non-unique login" do
      let!(:user2) { FactoryBot.create(:user) }

      before { user_attrs[:login] = user2.login }

      it "has an usuccessful response with HTTP status 400" do
        patch user_path(user.id), params: { user: user_attrs }, as: :json

        expect(response).to have_http_status(400)
        expect(response).not_to be_successful
      end

      it "does not update the user" do
        patch user_path(user.id), params: { user: user_attrs }, as: :json
        get user_path(user.id), as: :json

        expect(response_body["login"]).to eq user.login
      end
    end

    context "with negative salary" do
      before { user_attrs[:salary] = -1 }

      it "has an usuccessful response with HTTP status 400" do
        patch user_path(user.id), params: { user: user_attrs }, as: :json

        expect(response).to have_http_status(400)
        expect(response).not_to be_successful
      end

      it "does not update the user" do
        patch user_path(user.id), params: { user: user_attrs }, as: :json
        get user_path(user.id), as: :json

        expect(response_body["salary"]).to eq user.salary.to_s
      end
    end

    context "with invalid id", :realistic_error_responses do
      let!(:unknown_user) { FactoryBot.attributes_for(:user) }

      it "has an unsuccessful response with HTTP status 404" do
        patch user_path(unknown_user[:id]), params: { user: user_attrs }, as: :json

        expect(response).to have_http_status(404)
        expect(response).not_to be_successful
      end
    end
  end

  describe "DELETE /users/:id", :realistic_error_responses do
    context "existing user" do
      let!(:user) { FactoryBot.create(:user) }

      it "has a successful response with HTTP status 200" do
        delete user_path(user.id), as: :json

        expect(response).to have_http_status(200)
        expect(response).to be_successful
      end

      it "deletes the user" do
        delete user_path(user.id), as: :json
        get user_path(user.id), as: :json

        expect(response).to have_http_status(404)
        expect(response).not_to be_successful
      end
    end

    context "non-existent user" do
      let!(:unknown_user) { FactoryBot.attributes_for(:user) }

      it "has an unsuccessful response with HTTP status 404" do
        delete user_path(unknown_user[:id]), as: :json
        get user_path(unknown_user[:id]), as: :json

        expect(response).to have_http_status(404)
        expect(response).not_to be_successful
      end
    end
  end

  describe "visiting the /users page" do
    let!(:user) { FactoryBot.create(:user) }

    before { visit users_path }

    subject { page }

    it { is_expected.to have_content("Employees") }
    it { is_expected.to have_content("Min Salary") }
    it { is_expected.to have_content("Max Salary") }
    it { is_expected.to have_content("ID") }
    it { is_expected.to have_content("Login") }
    it { is_expected.to have_content("Name") }
    it { is_expected.to have_content("Salary") }

    it { is_expected.to have_content(user.id) }
    it { is_expected.to have_content(user.login) }
    it { is_expected.to have_content(user.name) }
    it { is_expected.to have_content(h.number_to_currency user.salary, unit: "S$") }
  end

  describe "visiting the /users/upload page" do
    before { visit upload_users_path }

    subject { page }

    it { is_expected.to have_content("Upload Employees from CSV") }
    it { is_expected.to have_content("Select File") }
    it { is_expected.to have_content("Upload") }
  end

  describe "visiting the users/:id page" do
    let!(:user) { FactoryBot.create(:user) }

    before { visit user_path(user.id) }

    subject { page }

    it { is_expected.to have_content(user.id) }
    it { is_expected.to have_content(user.name) }
    it { is_expected.to have_content(user.login) }
    it { is_expected.to have_content(h.number_to_currency user.salary, unit: "S$") }
  end
end
