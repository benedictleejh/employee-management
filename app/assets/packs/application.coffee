# This file is automatically compiled by Webpack, along with any other files
# present in this directory. You're encouraged to place your actual application logic in
# a relevant structure within app/javascript and only use these pack files to reference
# that code so it'll be compiled.

require('@rails/ujs').start()
require('turbolinks').start()
require('@rails/activestorage').start()
require 'channels'

import 'foundation-sites'
import 'motion-ui'

require 'javascripts/upload'

# Uncomment to copy all static images under ../images to the output folder and reference
# them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
# or the `imagePath` JavaScript helper below.
#
images = require.context('../images', true)
imagePath = (name) => images(name, true)

# Add jQuery for access on client side
window.jQuery = $;
window.$ = $;

$(document).on 'turbolinks:load', ->
  $ ->
    $(document).foundation()
    return
  return
