class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users, id: :string do |t|
      t.string :login, null: false
      t.string :name
      t.decimal :salary, precision: 10, scale: 2

      t.timestamps
    end

    add_index :users, :login, unique: true
  end
end
