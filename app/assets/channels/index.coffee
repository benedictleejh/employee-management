# Load all the channels within this directory and all subdirectories.
# Channel files must be named *_channel.js.

channels = require.context('.', true, /_channel\.coffee$/)
channels.keys().forEach channels
