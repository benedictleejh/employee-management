Rails.application.routes.draw do
  resources :users, except: [:new, :edit] do
    collection do
      match 'upload', via: [:get, :post]
    end
  end

  root 'users#index'
end
