# Employee Dashboard

## System Requirements

* Ruby 2.7.1 (tested on WSL, Ubuntu 20.04, installed via [rbenv](https://github.com/rbenv/rbenv))
* SQLite >= 3 (tested with 3.31.1)

## Development

To run the code locally, set up dependencies and the database with:

```
bundle install # Installs gems that are used in development and testing
yarn install # Install frontend dependencies
rails db:setup
```

To run the test suite:

```
rails spec
```

To run the site in a local server:

```
rails server
```

## Notes

* Salary validation attempts to follow the Robustness Principle; 0 to 2 decimal places are accepted as input, while
  output is strictly 2 decimal places
  * Salary being a decimal value with 2 decimal places in the database ensures we always output 2 decimal places

* The current upload handling ignores race conditions, so it is possible for a smaller uploaded file to be processed
  first and added to the database, despite being uploaded second
  * The most straightforward fix is likely a job queue to serialise the requests received, by pushing the validation
    and processing to the queue

* The HTML UI for the dashboard does not use the `limit` and `offset` parameters tha the JSON UI requires. While it can,
  paging is handled via [Kaminari](https://github.com/kaminari/kaminari) instead, for cleaner controller code
  * The page size is the same as the fixed limit of 30

* User creation is routed to `POST /users` instead of `POST /users/:id`, in accordance with general REST guidelines,
  and Ruby on Rails defaults
