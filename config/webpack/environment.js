const { environment } = require('@rails/webpacker')
const coffee =  require('./loaders/coffee')
const path = require('path')
const webpack = require("webpack")

// resolve-url-loader must be used before sass-loader
environment.loaders.get('sass').use.splice(-1, 0, {
  loader: 'resolve-url-loader'
});

environment.plugins.append("Provide", new webpack.ProvidePlugin({
  $: 'jquery',
  jQuery: 'jquery'
}))

environment.loaders.prepend('coffee', coffee)

module.exports = environment
