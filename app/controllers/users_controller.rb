require 'csv'

class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:upload]

  respond_to :html, :json

  before_action :clear_errs

  def index
    if request.format.html?
      @user = User.new

      min_salary = params[:minSalary] || 0
      max_salary = params[:maxSalary] || User.maximum("salary")
      offset = params[:offset]
      limit = params[:limit]
      sort = params[:sort] || "+id"

      if User.exists? &&
          validate_salary(min_salary, "Min salary") &&
          validate_salary(max_salary, "Max salary") &&
          validate_min_max_range(min_salary, max_salary) &&
          validate_sort(sort) &&
          (offset.nil? || validate_offset(offset)) &&
          (limit.nil? || validate_limit(limit)) then
        sort_order, sort_key = construct_sort_key_and_order(sort)
        @users =
          User
            .where(salary: BigDecimal(min_salary)..BigDecimal(max_salary))
            .limit(limit)
            .offset(offset)
            .order(sort_key => sort_order)
            .page params[:page]

        render locals: { sort_order: sort_order, sort_key: sort_key }
      else
        flash.now[:error] = @errs if !@errs.empty?
        @users = User.page params[:page]
        render locals: { sort_order: nil, sort_key: nil }
      end
    elsif request.format.json? && validate_params(params)
      sort_order, sort_key = construct_sort_key_and_order(params[:sort])
      @users =
        User
          .where(salary: BigDecimal(params[:minSalary])..BigDecimal(params[:maxSalary]))
          .limit(params[:limit])
          .offset(params[:offset])
          .order(sort_key => sort_order)

      respond_with @users
    else
      respond_to do |format|
        format.json { head :bad_request }
      end
    end
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        flash[:notice] = "Employee added successfully"
        format.js { render js: "window.location = '#{user_url(@user)}'" }
        format.html { redirect_to @user }
        format.json { render :show, status: :ok, location: @user }
      else
        format.js
        format.html { render partial: "users/new" }
        format.json { render json: @user.errors, status: :bad_request }
      end
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update(user_params)
        flash[:notice] = "Employee updated successfully"
        format.js { render js: "window.location = '#{user_url(@user)}'" }
        format.html { redirect_to @user }
        format.json { render :show, status: :ok, location: @user }
      else
        format.js
        format.html { render partial: "users/edit" }
        format.json { render json: @user.errors, status: :bad_request }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path, notice: "Employee #{@user.id} was successfully deleted."}
      format.json { head :ok }
    end
  end

  def upload
    if request.post?
      updated = false
      begin
        # In SQLite, a transaction automatically locks the entire DB
        User.transaction do
          csv_file = params[:file]
          csv_text = csv_file.read.force_encoding('utf-8').split("\n").select { |line| !line.start_with?("#") }.join("\n")
          csv = CSV.parse(csv_text, headers: true)
          @errs.push "File was empty" if csv.length == 0
          raise InvalidCSV if csv.length == 0
          raise InvalidCSV unless validate_csv csv

          csv.each do |row|
            user = User.where(id: row[0]).first_or_initialize
            user.login = row[1]
            user.name = row[2]
            user.salary = BigDecimal(row[3])
            user.save!
          end
        end

        updated = true
      rescue InvalidCSV
        updated = false
      end

      respond_to do |format|
        if updated
          format.html do
            flash.now[:notice] = "File successfully uploaded"
            render :upload, status: :ok
          end
          format.json { head :ok }
        else
          format.html do
            flash.now[:error] = @errs
            render :upload, status: :unprocessable_entity
          end
          format.json { head :unprocessable_entity }
        end
      end
    end
  end

  private

  ALLOWABLE_SORT_FIELDS = %w{name salary id login}
  SALARY_NUMBER_FORMAT = /\A\d+(?:\.\d{0,2})?\z/
  INTEGER_FORMAT = /\A\d+\z/

  class InvalidCSV < StandardError
  end

  def validate_csv(csv)
    all_ids, all_logins, row_lengths, salaries =
      csv.inject([[], [], [], []]) do |(ids, logins, row_lengths, salaries), row|
        [ids.push(row[0]), logins.push(row[1]), row_lengths.push(row.length), salaries.push(row[3])]
      end

    ids_uniq = all_ids.length == all_ids.uniq.length
    logins_uniq = all_logins.length == all_logins.uniq.length
    correct_row_lengths = row_lengths.all? { |i| i == 4 }
    salaries_format_correct = salaries.all? { |s| s =~ SALARY_NUMBER_FORMAT }

    @errs.push "Duplicated IDs exist" if !ids_uniq
    @errs.push "Duplicated logins exist" if !logins_uniq
    @errs.push "Row(s) with too many columns exist" if row_lengths.any? { |i| i > 4 }
    @errs.push "Row(s) with too few columns exist" if row_lengths.any? { |i| i < 4 }
    @errs.push "Row(s) with incorrectly formatted salaries exist" if !salaries_format_correct

    ids_uniq && logins_uniq && correct_row_lengths && salaries_format_correct
  end

  def validate_params(params)
    min_salary = params[:minSalary]
    max_salary = params[:maxSalary]
    offset = params[:offset]
    limit = params[:limit]
    sort = params[:sort]

    validate_salary(min_salary) &&
    validate_salary(max_salary) &&
    validate_min_max_range(min_salary, max_salary) &&
    validate_offset(offset) &&
    validate_limit(limit) &&
    validate_sort(sort)
  end

  def validate_salary(salary, param_name = nil)
    res = salary.to_s =~ SALARY_NUMBER_FORMAT

    @errs.push "#{param_name} #{salary} is not valid" if !res && !param_name.nil?

    res
  end

  def validate_min_max_range(min, max)
    res = BigDecimal(min) <= BigDecimal(max)

    @errs.push "Min must be less than or equal to max" if !res

    res
  end

  def validate_offset(offset)
    res = offset.to_s =~ INTEGER_FORMAT &&
      offset.to_i >= 0

    @errs.push "#{offset} is not a valid offset" if !res

    res
  end

  def validate_limit(limit)
    res = limit.to_s =~ INTEGER_FORMAT &&
      limit.to_i == 30

    @errs.push "#{limit} is not a valid limit" if !res

    res
  end

  def validate_sort(sort)
    res = sort.to_s =~ /\A(\+|\-)(#{ALLOWABLE_SORT_FIELDS.join("|")})\z/

    @errs.push "#{sort} is not a valid sort" if !res

    res
  end

  def construct_sort_key_and_order(sort)
    sort_order =
        if sort.first == '+'
          :asc
        elsif sort.first == '-'
          :desc
        end
      sort_key = sort[1..-1]

      [sort_order, sort_key]
  end

  def clear_errs
    @errs = []
  end

  def user_params
    params.require(:user).permit(:id, :name, :login, :salary)
  end
end
