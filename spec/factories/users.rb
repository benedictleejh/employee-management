FactoryBot.define do
  factory :user do
    sequence(:id) { |n| "e#{n.to_s.rjust 4, "0"}"}
    sequence(:login) { |n| "user#{n.to_s.rjust 4, "0"}"}
    sequence(:name) { |n| "User #{n.to_s.rjust 4, "0"}"}
    salary { Faker::Number.decimal(r_digits: 2) }
  end
end
