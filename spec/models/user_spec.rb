require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user = FactoryBot.create(:user)
  end

  subject { @user }

  it { is_expected.to respond_to(:id) }
  it { is_expected.to respond_to(:login) }
  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:salary) }

  it { is_expected.to be_valid }

  describe "validations" do
    it { is_expected.to validate_presence_of :id }
    it { is_expected.to validate_presence_of :login }
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :salary }

    it { is_expected.to validate_uniqueness_of(:id).case_insensitive }
    it { is_expected.to validate_uniqueness_of(:login).case_insensitive }

    context "negative salary" do
      before { @user.salary = -1 }

      it { is_expected.not_to be_valid }
    end
  end
end
