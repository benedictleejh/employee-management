module UsersHelper
  def sorted_users_path(request_params, current_sort_key, current_sort_order, new_sort_key)
    new_sort_order =
      if current_sort_key == new_sort_key && current_sort_order == :asc
        :desc
      elsif current_sort_key == new_sort_key && current_sort_order == :desc
        :asc
      else
        :asc
      end

    request_params["sort"] = make_sort_string(new_sort_key, new_sort_order)

    users_path(request_params.except("page"))
  end

  def make_sort_string(sort_key, sort_order)
    order = if sort_order == :desc
        "-"
      else
        "+"
      end

    "#{order}#{sort_key}"
  end

  def form_heading(form_action)
    case form_action
    in :edit
      "Edit employee"
    in :new
      "Add a new employee"
    end
  end
end
