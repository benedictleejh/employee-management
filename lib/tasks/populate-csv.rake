require 'faker'
require 'csv'

task :populate_csv, [:filename] do |t, args|
  users = (1..100).map do |n|
    id = "e#{n.to_s.rjust 4, "0"}"
    first_name = Faker::Name.first_name
    last_name = Faker::Name.last_name
    name = "#{first_name} #{last_name}"
    login = "#{first_name.first.downcase}#{last_name.gsub(/[^0-9a-z]/i, '').downcase}"
    salary = Faker::Number.decimal(r_digits: 2)

    [id, login, name, salary]
  end

  data = [["id","login","name","salary"]] + users

  File.write args[:filename], data.map(&:to_csv).join
end
