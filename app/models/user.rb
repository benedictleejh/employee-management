class User < ApplicationRecord
  validates :id, presence: true, uniqueness: { case_sensitive: false }
  validates :login, presence: true, uniqueness: { case_sensitive: false }
  validates :name, presence: true
  validates :salary, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
